use clap::clap_derive::ArgEnum;
use csv::WriterBuilder;
use env_logger::Env;
use log::debug;
use ndarray_csv::Array2Writer;

use gdal::{raster::RasterCreationOption, spatial_ref::SpatialRef, Driver};
use ndarray::{s, Array, Array3, Array4, ArrayView2, ArrayView3, Axis};
use num_traits::FromPrimitive;
use std::fs::File;
use std::ops::{Index, IndexMut};
use std::{
    env,
    fmt::Debug,
    ops::{Add, Div},
    path::{Path, PathBuf},
    process::Command,
};
use uuid::Uuid;

#[cfg(feature = "use_opencv")]
use opencv::{core::Mat, prelude::MatTraitConst, prelude::MatTraitConstManual};

pub type RasterData<T> = Array4<T>;
#[derive(Debug, PartialEq)]
pub enum CoordType {
    EInt(i64),
    EFloat(f32),
    EString(String),
}
use anyhow::Result;

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct RasterDataShape {
    pub times: usize,
    pub layers: usize,
    pub rows: usize,
    pub cols: usize,
}
//enum definitions
#[derive(Debug, std::cmp::PartialEq, Clone, Copy)]
pub enum Dimension {
    Layer,
    Time,
}

#[derive(Debug, Default, PartialEq, Clone, Copy)]
pub struct ImageResolution {
    pub x: f64,
    pub y: f64,
}

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub struct ImageSize {
    pub rows: usize,
    pub cols: usize,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Offset {
    pub rows: isize,
    pub cols: isize,
}
#[derive(PartialEq, Debug, Clone, Copy)]
pub struct Size {
    pub rows: isize,
    pub cols: isize,
}

#[derive(Debug, PartialEq, Clone, Copy)]
pub struct BlockSize {
    pub rows: usize,
    pub cols: usize,
}

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub struct GeoTransform {
    pub x_ul: f64,
    pub x_res: f64,
    pub x_rot: f64,
    pub y_ul: f64,
    pub y_rot: f64,
    pub y_res: f64,
}

#[derive(PartialEq, Debug, Clone, Copy)]
pub struct ReadWindow {
    pub offset: Offset,
    pub size: Size,
}

#[derive(Debug, PartialEq, Clone, Copy, Default)]
pub struct Overlap {
    // Describes how many cells have to be added to the read array
    // to produce a window. For example if overlap_size=1, then the top left
    // block will need one row to the left and one to the top to be added.
    // overlap should be {1,1,0,0}; for the lower left corner in general
    // overlap shoud be {0,0,1,1}
    pub left: usize,
    pub top: usize,
    pub right: usize,
    pub bottom: usize,
}

impl Default for Dimension {
    fn default() -> Self {
        Dimension::Layer
    }
}

impl Default for BlockSize {
    fn default() -> Self {
        BlockSize {
            rows: 1024,
            cols: 1024,
        }
    }
}

pub fn create_temp_file(ext: &str) -> String {
    let dir = env::var("TMP_DIR").unwrap_or("/tmp".to_string());
    let dir = Path::new(&dir);
    let file_name = format!("{}.{}", Uuid::new_v4(), ext);
    let file_name = dir.join(file_name);
    debug!("create_temp_file: {file_name:?}");
    file_name.into_os_string().into_string().unwrap()
}

//this function will warp a layer source to a different srs
pub(crate) fn warp(
    source: String,
    target_resolution: Option<ImageResolution>,
    target_epsg: u32,
) -> String {
    let new_source = create_temp_file("vrt");
    // make a vrt with the new epsg. I also need to think about the resampling algorithm.
    let mut cmd = Command::new("gdalwarp");
    cmd.arg("-q");
    if target_resolution.is_some() {
        cmd.args(&[
            "-tr",
            &format!("{}", target_resolution.unwrap().x),
            &format!("{}", target_resolution.unwrap().y),
        ]);
    }
    cmd.args(&["-t_srs", &format!("EPSG:{}", target_epsg)])
        .arg(source)
        .arg(&new_source)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    new_source
}

//this function will warp a layer source to a different srs
pub(crate) fn warp_with_te_tr(
    source: String,
    target_te: (f64, f64,f64,f64),
    target_resolution: ImageResolution
) -> String {
    let new_source = create_temp_file("vrt");
    // make a vrt with the new epsg. I also need to think about the resampling algorithm.
    let mut cmd = Command::new("gdalwarp");
    cmd.arg("-q");

    cmd.args(&[
        "-te",
        &format!("{}", target_te.0),
        &format!("{}", target_te.3),
        &format!("{}", target_te.2),
        &format!("{}", target_te.1),
    ])
    .args(&[
        "-tr",
        &format!("{}", target_resolution.x),
        &format!("{}", target_resolution.y),
    ])
        .arg(source)
        .arg(&new_source)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    new_source
}

pub(crate) fn change_res(source: String, target_resolution: ImageResolution) -> String {
    let new_source = create_temp_file("vrt");
    Command::new("gdalwarp")
        .arg("-q")
        .args(&[
            "-tr",
            &format!("{}", target_resolution.x),
            &format!("{}", target_resolution.y),
        ])
        .arg(source)
        .arg(&new_source)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    new_source
}
// This function is used internally to create a vrt
// to be used as layer source.
pub(crate) fn extract_band(source: String, band: usize) -> String {
    let new_source = create_temp_file("vrt");
    let argv = &[
        "gdal_translate",
        "-b",
        &format!("{}", band),
        "-q",
        &source,
        &new_source,
    ];

    std::process::Command::new(&argv[0])
        .args(&argv[1..])
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    new_source
}

// Creates and empty raster to store the ouptuts of a BlockWorker
pub fn raster_from_size<T, P>(
    file_name: P,
    geo_transform: GeoTransform,
    epsg_code: u32,
    block_size: BlockSize,
    n_bands: isize,
) where
    T: gdal::raster::GdalType + Copy + num_traits::identities::Zero,
    P: AsRef<Path>,
{
    let size_x = block_size.cols;
    let size_y = block_size.rows;
    let driver = Driver::get("GTIFF").unwrap();
    let options = [
        RasterCreationOption {
            key: "COMPRESS",
            value: "LZW",
        },
        RasterCreationOption {
            key: "BLOCKXSIZE",
            value: "512",
        },
        RasterCreationOption {
            key: "BLOCKYSIZE",
            value: "512",
        },
    ];
    let mut dataset = driver
        .create_with_band_type_with_options::<T, P>(
            file_name,
            size_x as isize,
            size_y as isize,
            n_bands,
            &options,
        )
        .unwrap();
    dataset
        .set_geo_transform(&geo_transform.to_array())
        .unwrap();
    let srs = SpatialRef::from_epsg(epsg_code).unwrap();
    dataset.set_spatial_ref(&srs).unwrap();
}

pub fn mosaic(collected: &[PathBuf], tmp_file: &Path) -> Result<()> {
    // build a vrt with all the collected files
    Command::new("gdalbuildvrt")
        .arg("-q")
        .arg(&tmp_file)
        .args(collected)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    Ok(())
}

pub fn translate(tmp_fn: &Path, image_fn: &Path) -> Result<()> {
    // translate to gtiff
    Command::new("gdal_translate")
        .arg("-q")
        .args(&["-of", "COG"])
        .args(&["-co", "BIGTIFF=YES"])
        .arg(tmp_fn)
        .arg(image_fn)
        .spawn()
        .expect("failed to start creating vrt")
        .wait()
        .expect("failed to wait for the vrt");
    Ok(())
}

#[derive(Debug, Copy, Clone, PartialEq, ArgEnum)]
pub enum SamplingMethod {
    Value,
    Mode,
}

// A location on an array
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Index2d {
    pub col: usize,
    pub row: usize,
}

#[derive(Default, Debug, PartialEq, Clone, Copy)]
pub(crate) struct Coordinates {
    // x and y coordinates of a location.
    pub(crate) x: f64,
    pub(crate) y: f64,
}

pub fn init_logger() {
    // Setup logger
    let env = Env::default().filter_or("LOG_LEVEL", "info");
    env_logger::init_from_env(env);
}

impl GeoTransform {
    pub fn to_array(&self) -> [f64; 6] {
        let array: [f64; 6] = [
            self.x_ul, self.x_res, self.x_rot, self.y_ul, self.y_rot, self.y_res,
        ];
        array
    }
}
pub fn rect_view<'a, T>(
    array: &'a ArrayView2<T>,
    rect: Rectangle,
    indices: Index2d,
) -> ArrayView2<'a, T> {
    let max_row = indices.row + rect.bottom + 1;
    let min_row = indices.row - rect.top;

    let max_col = indices.col + rect.right + 1;
    let min_col = indices.col - rect.left;

    array.slice(s![min_row..max_row, min_col..max_col])
}

#[derive(Debug, Copy, Clone)]
pub struct Rectangle {
    // I need better names for this!
    pub left: usize,
    pub top: usize,
    pub right: usize,
    pub bottom: usize,
}

pub fn write_csv_array(data: Vec<Vec<i16>>, csv_filename: PathBuf) {
    // Writes the array into the file.

    let rows = data.len();
    let cols = data[0].len();
    let flat: Vec<i16> = data.into_iter().flatten().collect();
    let array = Array::from(flat).into_shape((rows, cols)).unwrap();
    {
        let file = File::create(csv_filename).unwrap();
        let mut writer = WriterBuilder::new().has_headers(true).from_writer(file);
        writer.serialize_array2(&array).unwrap();
    }
}

pub fn trimm_array3<'a, T>(array: &'a Array3<T>, overlap_size: usize) -> ArrayView3<'a, T> {
    let min_row = overlap_size;
    let max_row = array.shape()[1] - overlap_size;

    let min_col = overlap_size;
    let max_col = array.shape()[2] - overlap_size;

    array.slice(s![.., min_row..max_row, min_col..max_col])
}

pub fn argmax<T: PartialOrd>(xs: &[T]) -> usize {
    if xs.len() == 1 {
        0
    } else {
        let mut maxval = &xs[0];
        let mut max_ixs: Vec<usize> = vec![0];
        for (i, x) in xs.iter().enumerate().skip(1) {
            if x > maxval {
                maxval = x;
                max_ixs = vec![i];
            } else if x == maxval {
                max_ixs.push(i);
            }
        }
        max_ixs[0]
    }
}

// translates an ArrayView2 to an opencv Mat.
#[cfg(feature = "use_opencv")]

pub fn arrayview2_to_mat<T>(data: ArrayView2<T>) -> Mat
where
    T: gdal::raster::GdalType + opencv::prelude::DataType,
{
    // transforms an Array2 into a opencv Mat data type.
    let rows = data.shape()[0];
    let data_slice = data.as_slice().unwrap();
    let m = Mat::from_slice(data_slice).unwrap();
    let m = m.reshape(0, rows as i32).unwrap();
    m
}

#[cfg(feature = "use_opencv")]
pub fn mat_to_arrayview2<T>(data: Mat) -> Array2<T>
where
    T: gdal::raster::GdalType + opencv::prelude::DataType,
{
    let rows = data.rows();
    let cols = data.cols();
    let result_vec_median: Vec<Vec<T>> = data.to_vec_2d().unwrap();
    let result_vec_median: Vec<T> = result_vec_median
        .iter()
        .flatten()
        .cloned()
        .map(|x| x as T)
        .collect();
    let result = Array2::from_shape_vec((rows as usize, cols as usize), result_vec_median).unwrap();
    result
}

pub trait Stack {
    fn stack(&mut self, other: RasterDataShape, dim_to_stack: Dimension) -> &mut RasterDataShape;
    fn extend(&mut self, other: RasterDataShape) -> &mut RasterDataShape;
}

impl Stack for RasterDataShape {
    fn extend(&mut self, other: RasterDataShape) -> &mut RasterDataShape {
        let mut extendable = true;

        for dim_loc in 1..4 {
            if self[dim_loc] != other[dim_loc] {
                extendable = false
            }
        }

        if extendable {
            self[0] += other[0];
            self
        } else {
            panic!("Unable to extend layers");
        }
    }

    fn stack(&mut self, other: RasterDataShape, dim_to_stack: Dimension) -> &mut RasterDataShape {
        let dimension_axis = dim_to_stack.get_axis();
        let mut stackable = true;
        for dim_loc in 0..4 {
            if dim_loc != dimension_axis && self[dim_loc] != other[dim_loc] {
                stackable = false;
            }
        }

        if stackable {
            self[dimension_axis] += other[dimension_axis];
            self
        } else {
            panic!("Unable to stack layers");
        }
    }
}

impl Dimension {
    pub fn get_axis(&self) -> usize {
        match self {
            Dimension::Layer => 1,
            Dimension::Time => 0,
        }
    }
}

impl Index<usize> for RasterDataShape {
    type Output = usize;

    fn index(&self, index: usize) -> &usize {
        match index {
            0 => &self.times,
            1 => &self.layers,
            2 => &self.rows,
            3 => &self.cols,
            n => panic!("Invalid index: {}", n),
        }
    }
}

impl IndexMut<usize> for RasterDataShape {
    fn index_mut(&mut self, index: usize) -> &mut usize {
        match index {
            0 => &mut self.times,
            1 => &mut self.layers,
            2 => &mut self.rows,
            3 => &mut self.cols,
            n => panic!("Invalid index: {}", n),
        }
    }
}

pub trait SumDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn sum_dimension(&self, dimension: Dimension) -> Array3<T>;
}

impl<T> SumDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn sum_dimension(&self, dimension: Dimension) -> Array3<T> {
        match dimension {
            Dimension::Layer => self.sum_axis(Axis(1)),
            Dimension::Time => self.sum_axis(Axis(0)),
        }
    }
}

pub(crate) trait MeanDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn mean_dimension(&self, dimension: Dimension) -> Array3<T>;
}

impl<T> MeanDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>,
{
    fn mean_dimension(&self, dimension: Dimension) -> Array3<T> {
        let mean = match dimension {
            Dimension::Layer => self.mean_axis(Axis(1)),
            Dimension::Time => self.mean_axis(Axis(0)),
        };
        mean.unwrap()
    }
}
pub trait VarDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn var_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T>;
}

impl<T> VarDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn var_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T> {
        match dimension {
            Dimension::Layer => self.var_axis(Axis(1), ddof),
            Dimension::Time => self.var_axis(Axis(0), ddof),
        }
    }
}

pub(crate) trait StdDimension<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn std_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T>;
}

impl<T> StdDimension<T> for RasterData<T>
where
    T: gdal::raster::GdalType
        + num_traits::identities::Zero
        + Copy
        + FromPrimitive
        + Add<Output = T>
        + Div<Output = T>
        + num_traits::Float,
{
    fn std_dimension(&self, ddof: T, dimension: Dimension) -> Array3<T> {
        match dimension {
            Dimension::Layer => self.std_axis(Axis(1), ddof),
            Dimension::Time => self.std_axis(Axis(0), ddof),
        }
    }
}

pub fn trimm_array4<T>(array: &Array4<T>, overlap_size: usize) -> ndarray::ArrayView4<T> {
    let min_row = overlap_size;
    let max_row = array.shape()[2] - overlap_size;

    let min_col = overlap_size;
    let max_col = array.shape()[3] - overlap_size;
    let slice = s![.., .., min_row..max_row, min_col..max_col];
    array.slice(slice)
}

// pub trait Select<T>
// where
//     T: gdal::raster::GdalType
//         + num_traits::identities::Zero
//         + Copy
//         + FromPrimitive
//         + Add<Output = T>
//         + Div<Output = T>,
// {
//     fn select_coords(
//         &self,
//         coord_name: &str,
//         coord_values: Vec<CoordType>,
//     ) -> ArrayBase<ViewRepr<&T>, IxDyn>; // -> ArrayView<T, D>;
// }

// impl<T> Select<T> for LabeledRasterData<T>
// where
//     T: gdal::raster::GdalType
//         + num_traits::identities::Zero
//         + Copy
//         + Debug
//         + FromPrimitive
//         + Add<Output = T>
//         + Div<Output = T>,
// {
//     fn select_coords(
//         &self,
//         _dimension_name: &str,
//         _coord_values: Vec<CoordType>,
//     ) -> ArrayBase<ViewRepr<&T>, IxDyn> {
//         todo!();
//         // println!("Availabale dimensions: {:?}", self.dimensions);
//         // println!("Dimension to select: {:?}", dimension_name);
//         // let index_dimension = self
//         //     .dimensions
//         //     .iter()
//         //     .position(|d| d == dimension_name)
//         //     .unwrap();
//         // println!("index_dimension: {:?}", index_dimension);
//         // println!("coord_values to select: {:?}", coord_values);

//         // let selected_coord = &self.coords[index_dimension];
//         // println!("selected_coord: {:?}", selected_coord);
//         // let mut index_coords = Vec::new();
//         // for c in coord_values {
//         //     let sel = selected_coord.iter().position(|sc| sc == &c ).unwrap();
//         //     index_coords.push(sel);
//         // }
//         // println!("index_coords: {:?}", index_coords);

//         // let res = self.values.slice(s![0_i32, 0_i32, .., ..]).into_dyn();
//         // res
//     }
// }

// #[derive(Debug)]
// struct Coords {
//     coords: Vec<CoordType>,
// }

// #[derive(Debug)]
// pub struct LabeledRasterData<T> {
//     pub(crate) values: RasterData<T>,
//     pub(crate) dimensions: Vec<String>, // the names ['time', 'layer']
//     pub(crate) coords: Vec<Vec<CoordType>>,
// }
