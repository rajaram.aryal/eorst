use crate::RasterDataset;
use colored::*;

impl std::fmt::Display for RasterDataset {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::result::Result<(), std::fmt::Error> {
        // writeln!(fmt, "{} \t {}", "Source: ".bold(), self.source).unwrap();

        // writeln!(fmt, "{}", "Coordinates: ".bold()).unwrap();

        // writeln!(fmt, "\t {}", "x ".bold()).unwrap();
        // writeln!(fmt, "\t {}", "y ".bold()).unwrap();
        // writeln!(fmt, "\t {} \t \t \t {:?}", "Bands".bold(), self.bands).unwrap();

        writeln!(fmt, "{}", "Attributes: ".bold()).unwrap();
        writeln!(
            fmt,
            "\t {} \t \t ( times: {:?}, layers: {:?}, rows: {:?}, cols: {:?} )",
            "image_size: ".bold(),
            self.metadata.shape.times,
            self.metadata.shape.layers,
            self.metadata.shape.rows,
            self.metadata.shape.cols,
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t \t ( {:?}, {:?} )",
            "block_size: ".bold(),
            self.metadata.block_size.rows,
            self.metadata.block_size.cols
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t \t {:?}",
            "n_blocks: ".bold(),
            self.blocks.len(),
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t {:?}",
            "geo_transform: ".bold(),
            self.metadata.geo_transform
        )
        .unwrap();
        writeln!(
            fmt,
            "\t {} \t \t {:?} ",
            "epsg_code: ".bold(),
            self.metadata.epsg_code
        )
    }
}
