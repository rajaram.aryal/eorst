//! # An earth observation and remote sensig toolkit written in rust.
//!
//! ## Summary
//! This crate offers a library aiming to simplify the writing of raster processing pipelines in rust.
//!
//! ## Highlights
//!
//!  - Parallel read/write.
//!  - On-the-fly image reprojection.
//!  - Point raster sampling extraction.
//!  - Time series analysis.
//!  - Band math.
//!  - Basic opencv integrtion
//!  - Easy to integrate with LigthGBM and XGBoost.
//!
//!
//! ## Crate Status
//!
//! - Still iterating on and evolving the crate.
//!   + The crate is continuously developing, and breaking changes are expected
//!     during evolution from version to version.
//!
//! ## Details
//!
//! EORST is an open-source library aiming to fill a void in the rust ecosystem, that is the lack of a library
//! to simplify writing geospatial processing code. It takes inspiration from well-established python libraries such as rasterio, rios, and dask and implements some
//! of them in rust, enabling the programmer to focus on the geospatial process rather than the implementation details while taking advantage of the benefits of
//! the rust programming language.
//!
//!  Despite being in early stages of development eorst already offers a rich set of tools such as simple reading and writing of geospatial data
//!  on-the-fly projection and alignment of input data, partition input data into small blocks allowing computation on arrays larger than memory concurrently,
//!  efficient raster point sampling, simple band math, time series analysis and integration with 3rd party libraries like such as opencv to perform computer
//!  vision task and LightGBM and XGBoost for machine learning. The library is available in the official creates registry <https://crates.io/crates/eorst>.
//!
//!  The main focus of this library is to support the work of the [JRSRP](https://www.jrsrp.org.au/), but I expect it to be  useful for a wider audience.
//!  Most of the current functionalities were developed to support the [Spatial Biocondition project](https://www.qld.gov.au/__data/assets/pdf_file/0015/230019/spatial-biocondition-vegetation-condition-map-for-queensland.pdf), developed jointly by Quenslands Remote Sensing Centre
//!  and the Quensland Herbarium.
//!
//!  Future work in the short term will be focused on the stabilization of the API and extending the documentation and examples, collaboration is welcome.
//!
//! ## How to use:
//!
//! ### As a library:
//!
//! Just add eotrs to your dependencies in `cargo.toml` like:
//!
//! ```toml
//! [dependencies]
//! eotrs = {git = https://gitlab.com/leohardtke/eotrs"}
//! ```
//!
//! [RasterDatasets](RasterDataset) are the main data structre and functionality provider of the crate.
//!
//! ### As a cli tool
//!
//! If you want to use the comman line interface see [install](standalonde_docs/install/index.html) for instructions.
//!
//!
//! - Examples:
//!
//! ```bash
//! eorst extract data/cemsre_t55jfm_20200614_sub_abam5.tif\
//!         data/cemsre_t55jfm_20200614_sub_abam5.gpkg\
//!         id\
//!         test.csv\
//!         -b 1,2,3\
//!         -s 256\
//!         -n 8
//!

use std::{
    cmp::{max, min},
    collections::BTreeMap,
    fmt::Debug,
    fs,
    path::{Path, PathBuf},
    vec,
};
pub mod utils;

use gdal::{raster::Buffer, vector::FieldValue, Dataset, DatasetOptions, GdalOpenFlags};
use math::round;
use ndarray::{s, Axis};
use ndarray::{Array2, Array3, Array4};
use utils::*;
mod tests;
use itertools::Itertools;
use rayon::iter::{
    IndexedParallelIterator, IntoParallelIterator, IntoParallelRefIterator, ParallelIterator,
};
pub mod standalonde_docs;

pub mod rasterdataset;
pub use rasterdataset::{RasterDataset, RasterDatasetBuilder};

use log::{debug, warn};
extern crate env_logger;
// use gdal::vector::LayerAccess;

#[cfg(feature = "use_lightgbm")]
use lightgbm;
#[cfg(feature = "use_lightgbm")]
use serde_json::Value;

#[derive(Debug, Clone, PartialEq)]
pub struct Layer {
    source: String,
    pub(crate) layer_pos: usize,
    pub(crate) time_pos: usize,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterBlock {
    pub block_index: usize,
    pub read_window: ReadWindow,
    pub overlap_size: usize,
    pub geo_transform: GeoTransform,
    pub overlap: Overlap,
    pub shape: RasterDataShape,
    pub epsg_code: usize,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterMetadata {
    pub layers: Vec<Layer>,
    pub shape: RasterDataShape,
    pub(crate) block_size: BlockSize,
    epsg_code: u32,
    geo_transform: GeoTransform,
    overlap_size: usize,
    pub time_indices: Vec<usize>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct RasterDatasetIter<'a> {
    pub parent: &'a RasterDataset,
    pub block: &'a RasterBlock,
    pub iter_index: usize,
}

impl Drop for RasterDataset {
    fn drop(&mut self) {
        for tmp_layer in &self.tmp_layers {
            let to_remove = tmp_layer;
            debug!("Dropping {to_remove:?}");
            fs::remove_file(to_remove).unwrap();
        }
    }
}

#[derive(Debug, PartialEq, Clone)]
pub struct DataSource {
    pub source: String,
    pub bands: Vec<usize>,
    // pub names: Vec<String>,
}

pub struct DataSourceBuilder {
    pub source: String,
    pub bands: Vec<usize>,
    // pub names: Vec<String>,
}

impl DataSourceBuilder {
    pub fn from_file(file_name: &PathBuf) -> Self {
        let source = file_name.clone().into_os_string().into_string().unwrap();
        debug!("Adding {source} to datasource");
        let n_bands = DataSourceBuilder::get_n_bands(file_name);
        let bands: Vec<usize> = (1..n_bands + 1).map(|b| b as usize).collect();

        DataSourceBuilder { source, bands }
    }
    pub fn build(self) -> DataSource {
        DataSource {
            source: self.source,
            bands: self.bands,
        }
    }
    pub fn bands(mut self, bands: Vec<usize>) -> Self {
        self.bands = bands;
        self
    }
    fn get_n_bands(source: &PathBuf) -> isize {
        let ds = Dataset::open(source).unwrap();
        ds.raster_count()
    }
}

impl<'a> RasterDatasetBuilder {
    pub fn from_source(data_source: &DataSource) -> RasterDatasetBuilder {
        RasterDatasetBuilder::from_sources(&vec![data_source.to_owned()])
    }
    pub fn from_sources(data_sources: &Vec<DataSource>) -> RasterDatasetBuilder {
        let mut sources: Vec<String> = Vec::new();
        let mut bands = Vec::new();

        let geo_transform = GeoTransform::default();
        let image_size = ImageSize::default();
        let block_size = BlockSize::default();

        let epsg = RasterDatasetBuilder::get_epsg_code(&data_sources[0].source);
        let overlap_size = 0;

        // default behaviour for compositions
        let composition_bands = Dimension::Layer;
        let composition_sources = Dimension::Time; // as many times as sources

        for data_source in data_sources {
            sources.push(data_source.source.to_string());
            bands.push(data_source.bands.clone());
        }

        let time_indices = (0..sources.len()).collect();
        let resolution = RasterDatasetBuilder::get_resolution(&data_sources[0].source);

        RasterDatasetBuilder {
            sources,
            bands,
            epsg,
            overlap_size,
            block_size,
            composition_bands,
            composition_sources,
            image_size,
            geo_transform,
            time_indices,
            resolution,
        }
    }

    /// Set the desired epsg code for the rasterdataset.
    pub fn set_epsg(mut self, epsg_code: u32) -> Self {
        self.epsg = epsg_code;
        self
    }

    //Set the desired geo_transform
    pub fn set_geo_transform(mut self, geo_transform: GeoTransform) -> Self {
        self.geo_transform = geo_transform;
        self
    }

    /// Set the desired resolution for the rasterdataset.
    pub fn set_resolution(mut self, resolution: ImageResolution) -> Self {
        self.resolution = resolution;
        self
    }
    fn get_resolution(source: &str) -> ImageResolution {
        let source: &Path = Path::new(&source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let geo_transform = ds.geo_transform().unwrap();
        ImageResolution {
            x: geo_transform[1],
            y: geo_transform[5],
        }
    }

    fn get_geotransform(source: &str) -> GeoTransform {
        let source: &Path = Path::new(&source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let geo_transform = ds.geo_transform().unwrap();
        GeoTransform {
            x_ul: geo_transform[0],
            x_res: geo_transform[1],
            x_rot: geo_transform[2],
            y_ul: geo_transform[3],
            y_rot: geo_transform[4],
            y_res: geo_transform[5],
        }
    }

    fn get_epsg_code(source: &str) -> u32 {
        let source: &Path = Path::new(source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let spatial_ref = ds.spatial_ref().unwrap();
        let epsg_code = spatial_ref.auth_code().unwrap_or(3755); //this is just a workaround. I need to implement OSRFindMatches in gdal
        epsg_code.try_into().unwrap()
    }

    fn get_image_size(source: &str) -> ImageSize {
        let source: &Path = Path::new(source);
        let ds: Dataset = Dataset::open(source).unwrap();
        let size = ds.raster_size(); // x, y
        ImageSize {
            rows: size.1,
            cols: size.0,
        }
    }

    fn get_blocks(
        &'a self,
        block_shape: RasterDataShape,
        layers: &Vec<Layer>,
        epsg_code: usize,
    ) -> Vec<RasterBlock> {
        let n_blocks = self.n_blocks();
        let mut blocks: Vec<RasterBlock> = Vec::new();
        (0..n_blocks).into_iter().for_each(|i| {
            let block_attributes = self.block_from_id(i, block_shape, layers, epsg_code);
            blocks.push(block_attributes)
        });
        blocks
    }

    fn n_block_cols(&self) -> usize {
        let image_size = self.image_size;
        let block_size = self.block_size;
        round::ceil(image_size.cols as f64 / block_size.cols as f64, 0) as usize
    }

    fn n_block_rows(&self) -> usize {
        let image_size = self.image_size;
        let block_size = self.block_size;
        round::ceil(image_size.rows as f64 / block_size.rows as f64, 0) as usize
    }

    fn n_blocks(&self) -> usize {
        self.n_block_cols() * self.n_block_rows()
    }

    // Images are chuncked in blocks. This function calculates
    // how many columns and rows of blocks an image has.
    // i.e a 1024x1024 image chuncked in 512x512 blocks
    // will have 2 block columns.
    fn block_col_row(&self, id: usize) -> (usize, usize) {
        let block_row = id as usize / self.n_block_cols();
        let block_col = id as usize - (block_row * self.n_block_cols());

        (block_col, block_row)
    }

    fn block_from_id(
        &'a self,
        id: usize,
        block_shape: RasterDataShape,
        _layers: &Vec<Layer>,
        epsg_code: usize,
    ) -> RasterBlock {
        let mut overlap = Overlap {
            left: 0,
            top: 0,
            right: 0,
            bottom: 0,
        };
        // get ul corner of the block
        let ul_x = (self.block_size.cols * self.block_col_row(id).0) as isize;
        let ul_y = (self.block_size.rows * self.block_col_row(id).1) as isize;
        // now compute with overlap
        let ul_x_overlap = max(0, ul_x - self.overlap_size as isize);
        let ul_y_overlap = max(0, ul_y - self.overlap_size as isize);
        let lr_x_overlap = min(
            self.image_size.cols as isize,
            ul_x + self.block_size.cols as isize + self.overlap_size as isize,
        );
        let lr_y_overlap = min(
            self.image_size.rows as isize,
            ul_y + self.block_size.rows as isize + self.overlap_size as isize,
        );

        let win_width = lr_x_overlap - ul_x_overlap;
        let win_height = lr_y_overlap - ul_y_overlap;

        let arr_width = win_width;
        let arr_height = win_height;

        let read_window = ReadWindow {
            offset: Offset {
                cols: ul_x_overlap,
                rows: ul_y_overlap,
            },
            size: Size {
                cols: arr_width,
                rows: arr_height,
            },
        };

        // add borders
        if (ul_x - self.overlap_size as isize) < 0 {
            // add left
            overlap.left = self.overlap_size;
        }
        if (ul_y - self.overlap_size as isize) < 0 {
            // add top
            overlap.top = self.overlap_size;
        }
        if (self.image_size.cols as isize)
            < (ul_x + self.block_size.cols as isize + self.overlap_size as isize)
        {
            // add right
            overlap.right = self.overlap_size;
        }

        if (self.image_size.rows as isize)
            < (ul_y + self.block_size.rows as isize + self.overlap_size as isize)
        {
            // add bottom
            overlap.bottom = self.overlap_size;
        }

        let block_geo_transform = self.get_block_gt(read_window);

        RasterBlock {
            block_index: id,
            read_window,
            overlap_size: self.overlap_size,
            geo_transform: block_geo_transform,
            overlap,
            shape: block_shape,
            epsg_code,
        }
    }

    fn get_block_gt(&self, read_window: ReadWindow) -> GeoTransform {
        let x_ul_image = self.geo_transform.x_ul;
        let y_ul_image = self.geo_transform.y_ul;

        let x_res = self.geo_transform.x_res;
        let y_res = self.geo_transform.y_res;
        let x_pos = read_window.offset.cols;
        let y_pos = read_window.offset.rows;

        let x_ul_block = x_ul_image + x_res * x_pos as f64; // to fix! will break with overlap
        let y_ul_block = y_ul_image + y_res * y_pos as f64; // to fix! will break with overlap

        GeoTransform {
            x_ul: x_ul_block,
            x_res,
            x_rot: self.geo_transform.x_rot,
            y_ul: y_ul_block,
            y_rot: self.geo_transform.x_rot,
            y_res,
        }
    }

    /// sets the block size of the raster dataset
    pub fn block_size(mut self, block_size: BlockSize) -> RasterDatasetBuilder {
        self.block_size = block_size;
        self
    }
    pub fn source_composition_dimension(
        mut self,
        composition_dimension: Dimension,
    ) -> RasterDatasetBuilder {
        self.composition_sources = composition_dimension;
        self
    }

    pub fn band_composition_dimension(
        mut self,
        composition_dimension: Dimension,
    ) -> RasterDatasetBuilder {
        self.composition_bands = composition_dimension;
        self
    }

    pub fn set_time_indices(mut self, time_indices: Vec<usize>) -> RasterDatasetBuilder {
        self.time_indices = time_indices;
        self
    }

    pub fn overlap_size(mut self, overlap_size: usize) -> RasterDatasetBuilder {
        self.overlap_size = overlap_size;
        self
    }

    
    pub fn set_template(mut self, other: &RasterDataset) -> RasterDatasetBuilder {
        let current_epsg = RasterDatasetBuilder::get_epsg_code(&self.sources[0]);
        let target_epsg = self.epsg;
        if current_epsg != target_epsg {
            warn!("Warning epsg was set, but does not match the epsg of the template.");
            self.epsg = other.metadata.epsg_code;
        }
       
        self.image_size = ImageSize {rows : other.metadata.shape.rows,
                                      cols: other.metadata.shape.cols};
        self.geo_transform = other.metadata.geo_transform;

        self
    }

    pub fn build(mut self) -> RasterDataset {
        let mut layers = Vec::new();
        let mut tmp_layers: Vec<String> = Vec::new();
        let mut blocks = Vec::new();

        for source in self.sources.iter_mut() {
            let current_epsg = RasterDatasetBuilder::get_epsg_code(source);
            let target_epsg = self.epsg;

            let current_resoultion = RasterDatasetBuilder::get_resolution(source);
            let target_resolution = self.resolution;

            if current_epsg != target_epsg {
                *source = warp(source.to_string(), None, target_epsg);
                tmp_layers.push(source.clone());
            }

            if current_resoultion != target_resolution {
                *source = change_res(source.to_string(), target_resolution);
                tmp_layers.push(source.clone());
            }

            
        }

        for source in self.sources.iter_mut() {
            let current_gt =RasterDatasetBuilder::get_geotransform(&source);
            let target_gt = self.geo_transform;            
            if (current_gt != target_gt) & (target_gt!= GeoTransform::default()) {
                let x_ll = target_gt.x_ul + (self.image_size.cols as f64 * target_gt.x_res);
                let y_ll = target_gt.y_ul + (self.image_size.rows as f64 * target_gt.y_res);
                let te = (target_gt.x_ul,target_gt.y_ul,x_ll, y_ll);
                let tr = ImageResolution{x: target_gt.x_res, y: target_gt.y_res.abs()};
                *source = warp_with_te_tr(source.to_string(), te, tr);
                tmp_layers.push(source.clone());
            }
        }
        
        let geo_transform = RasterDatasetBuilder::get_geotransform(&self.sources[0]);


        let image_size = RasterDatasetBuilder::get_image_size(&self.sources[0]);
        for source in self.sources.iter() {
            let gt = RasterDatasetBuilder::get_geotransform(source);
            let is = RasterDatasetBuilder::get_image_size(source);
            if gt != geo_transform {
                panic!("Sources have different geo_transforms!");
            }
            if is != image_size {
                panic!("Sources have different size!")
            }
        }

        self.geo_transform = geo_transform;
        self.image_size = image_size;

        let n_rows = image_size.rows;
        let n_cols = image_size.cols;
        let mut time_pos = 1;
        let mut layer_pos = 1;
        for (source_idx, source) in self.sources.iter().enumerate() {
            for (_, band) in self.bands[source_idx].iter().enumerate() {
                let source = source;
                let new_source = extract_band(source.to_string(), *band);
                tmp_layers.push(new_source.clone());
                let source = new_source;
                let layer = Layer {
                    source,
                    time_pos: time_pos - 1,
                    layer_pos: layer_pos - 1,
                };
                layers.push(layer);

                match self.composition_bands {
                    Dimension::Layer => layer_pos += 1,
                    Dimension::Time => time_pos += 1,
                }
            }

            match self.composition_sources {
                Dimension::Layer => {
                    if self.composition_bands != Dimension::Layer {
                        layer_pos += 1
                    }
                }
                Dimension::Time => {
                    if self.composition_bands != Dimension::Time {
                        time_pos += 1
                    }
                }
            }

            match self.composition_sources {
                Dimension::Layer => time_pos = 1,
                Dimension::Time => layer_pos = 1,
            }
        }

        let n_times = layers.iter().map(|l| l.time_pos).max().unwrap() + 1;
        let n_layers = layers.iter().map(|l| l.layer_pos).max().unwrap() + 1;

        let block_shape = RasterDataShape {
            times: n_times,
            layers: n_layers,
            rows: n_rows,
            cols: n_cols,
        };

        blocks.extend(self.get_blocks(block_shape, &layers, self.epsg.try_into().unwrap()));

        let metadata = RasterMetadata {
            layers,
            shape: block_shape, //FIXME?
            block_size: self.block_size,
            epsg_code: self.epsg,
            geo_transform,
            overlap_size: self.overlap_size,
            time_indices: self.time_indices,
        };
        RasterDataset {
            metadata,
            blocks,
            tmp_layers,
        }
    }
}

impl RasterDataset {
    /// An iterator over the [RasterBlock]s of a [RasterDataset].

    pub fn iter(&self) -> RasterDatasetIter<'_> {
        let iter_index = 0;
        RasterDatasetIter {
            parent: self,
            block: &self.blocks[iter_index],
            iter_index,
        }
    }

    /// Applays a worker function over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.
    pub fn process<T>(&self, worker: fn(&Array4<T>) -> Array4<T>, n_cpus: usize, out_file: &Path)
    where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks.to_owned().into_par_iter().map(
                |raster_block: RasterBlock| -> Vec<PathBuf> {
                    //(Vec<usize>, Vec<PathBuf>) {
                    let bid = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let mut block_data = self.read_block::<T>(bid);
                    let result = worker(&mut block_data);
                    let mut block_fns: Vec<PathBuf> = Vec::new();
                    for (tid, result3) in result.axis_iter(Axis(0)).enumerate() {
                        let block_fn =
                            tmp_file.with_file_name(format!("{}_{}_{}.tif", file_stem, tid, bid));
                        raster_block.write3(result3.to_owned(), &block_fn);
                        block_fns.push(block_fn);
                    }
                    block_fns
                },
            )
        });

        let collected: Vec<Vec<PathBuf>> = handle.collect();
        (0..self.metadata.shape.times)
            .into_par_iter()
            .for_each(|time_index| {
                let mut block_fns = Vec::new();
                for block in collected.iter() {
                    let block_fn = block[time_index].to_owned();
                    block_fns.push(block_fn);
                }
                let tmp_file = PathBuf::from(create_temp_file("vrt"));
                let file_stem = out_file.file_stem().unwrap().to_str().unwrap();
                let time_fn = out_file.with_file_name(format!("{}_{}.tif", file_stem, time_index));
                mosaic(&block_fns, &tmp_file).expect("Could not mosaic to vrt");
                translate(&tmp_file, &time_fn).expect("Could not translate to geotiff");
                fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
                block_fns
                    .iter()
                    .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
            });
    }

    /// Applays a worker function that reduces the data dimensionality over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.
    ///
    pub fn reduce<T>(
        &self,
        worker: fn(&Array4<T>, Dimension) -> Array3<T>,
        dimension: Dimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .map(|raster_block: RasterBlock| -> PathBuf {
                    let id = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let mut block_data = self.read_block::<T>(id);
                    let result = worker(&mut block_data, dimension);
                    self.write_window3(id, result, &block_fn);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file).expect("Could not mosaic to vrt");
        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    #[cfg(feature = "use_lightgbm")]
    pub fn lightgbm_fit_classifier(
        &self,
        training_data: &PathBuf,
        params: Value,
        class_column: &str,
    ) {
        let data = self.extract_blockwise(&training_data, "id", SamplingMethod::Value, None);
        let class = get_class(&training_data, "id", class_column);

        let y: Vec<f32> = class.iter().map(|(_, v)| *v as f32).collect();
        let x: Vec<Vec<f64>> = data
            .into_iter()
            .map(|(_, v)| v.iter().map(|v| *v as f64).collect())
            .collect();
        let train_dataset = lightgbm::Dataset::from_mat(x.clone(), y).unwrap();

        let booster = lightgbm::Booster::train(train_dataset, &params).unwrap();

        booster.save_file("test.mod");
    }

    #[cfg(feature = "use_lightgbm")]
    pub fn lightgbm_predict(&self, out_file: &PathBuf) {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(1)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));
        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .map(|raster_block: RasterBlock| -> PathBuf {
                    let booster = lightgbm::Booster::from_file("test.mod").unwrap();
                    let id = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let block_data = raster_block.read::<f64>();
                    let features = block_data
                        .t()
                        .into_shape((
                            block_data.shape()[2] * block_data.shape()[3],
                            block_data.shape()[1],
                        ))
                        .unwrap();
                    let mut features_vec = Vec::new();
                    for row in features.rows() {
                        features_vec.push(row.to_vec());
                    }
                    let predictions = booster.predict(features_vec).unwrap();
                    let predictions_class: Vec<u8> =
                        predictions.iter().map(|p| argmax(p) as u8).collect();

                    let result = Array::from_shape_vec(
                        (raster_block.shape.rows * raster_block.shape.cols, 1),
                        predictions_class,
                    )
                    .unwrap();
                    raster_block.write_samples_feature(&result, &block_fn);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file).expect("Could not mosaic to vrt");
        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    /// Applays a worker function over each [RasterBlock] of a [RasterDataset] in parallel using `n_cpus`.

    pub fn reduce_row_pixel<T>(
        &self,
        worker: fn(&Array4<T>, Dimension) -> Array2<T>,
        dimension: Dimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .map(|raster_block: RasterBlock| -> PathBuf {
                    let id = raster_block.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let mut block_data = self.read_block::<T>(id);
                    let result = worker(&mut block_data, dimension);
                    raster_block.write_samples_feature(&result, &block_fn);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file).expect("Could not mosaic to vrt");

        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    #[doc(hidden)]
    pub fn write_window3<T, P>(&self, block_index: usize, data: Array3<T>, out_fn: P)
    where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + Debug,
        P: AsRef<Path> + Copy,
    {
        //
        let trimmed = trimm_array3(&data, self.blocks[block_index].overlap_size);

        // create and empty raster with the right size
        let block_geotransform = self.blocks[block_index].geo_transform;
        let block_size: BlockSize = BlockSize {
            // rows: self.blocks[block_index].read_window.size.rows as usize,
            // cols: self.blocks[block_index].read_window.size.cols as usize,
            rows: trimmed.shape()[1],
            cols: trimmed.shape()[2],
        };
        let dataset_options: DatasetOptions = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            allowed_drivers: None,
            open_options: None,
            sibling_files: None,
        };
        let n_bands = data.shape()[0] as isize;
        let epsg_code = self.metadata.epsg_code;
        raster_from_size::<T, P>(out_fn, block_geotransform, epsg_code, block_size, n_bands);
        let out_ds = Dataset::open_ex(out_fn, dataset_options).unwrap();
        for band in 0..data.shape()[0] {
            let b = (band + 1) as isize;
            let mut out_band = out_ds.rasterband(b).unwrap();
            let data_vec: Vec<T> = trimmed
                .slice(s![band, .., ..])
                .into_iter()
                .copied()
                .collect();
            let data_buffer = Buffer {
                size: (block_size.cols, block_size.rows),
                data: data_vec,
            };

            out_band
                .write((0, 0), (block_size.cols, block_size.rows), &data_buffer)
                .unwrap();
        }
    }

    pub fn reduce_with_mask<T, U>(
        &self,
        other: &RasterDataset,
        worker: fn(&Array4<T>, &Array4<U>, Dimension) -> Array3<T>,
        dimension: Dimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        // todo!: Using first ds as a templete. This should be actually saved as mrd attributes!

        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .zip(other.blocks.to_owned().into_par_iter())
                .map(|(raster_block_data, _raster_block_mask)| -> PathBuf {
                    let id = raster_block_data.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!("{}_{}.tif", file_stem, id));
                    let mut block_data = self.read_block::<T>(id);
                    let mask = other.read_block::<U>(id);
                    let result = worker(&mut block_data, &mask, dimension);
                    raster_block_data.write3(result, &block_fn);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file).expect("Could not mosaic to vrt");

        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    pub fn reduce_row_pixel_with_mask<T, U>(
        &self,
        other: &RasterDataset,
        worker: fn(&Array4<T>, &Array4<U>, Dimension) -> Array2<T>,
        dimension: Dimension,
        n_cpus: usize,
        out_file: &Path,
    ) where
        T: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
        U: gdal::raster::GdalType + Copy + num_traits::Zero + Debug,
    {
        // todo!: Using first ds as a templete. This should be actually saved as mrd attributes!

        let pool = rayon::ThreadPoolBuilder::new()
            .num_threads(n_cpus)
            .build()
            .unwrap();

        // this will be used to save the intermediate vrt and block files
        let tmp_file = PathBuf::from(create_temp_file("vrt"));

        let handle = pool.install(|| {
            self.blocks
                .to_owned()
                .into_par_iter()
                .zip(other.blocks.to_owned().into_par_iter())
                .map(|(raster_block_data, _raster_block_mask)| -> PathBuf {
                    let id = raster_block_data.block_index;
                    let file_stem = tmp_file.file_stem().unwrap().to_str().unwrap();
                    let block_fn = tmp_file.with_file_name(format!(
                        "{}_{}.tif",
                        file_stem, raster_block_data.block_index
                    ));
                    let mut block_data = self.read_block::<T>(id);
                    let mask = other.read_block::<U>(id);
                    let result = worker(&mut block_data, &mask, dimension);
                    raster_block_data.write_samples_feature(&result, &block_fn);
                    block_fn
                })
        });

        let collected: Vec<PathBuf> = handle.collect();
        mosaic(&collected, &tmp_file).expect("Could not mosaic to vrt");

        translate(&tmp_file, out_file).expect("Could not translate to geotiff");

        // remove all temp files
        fs::remove_file(tmp_file).expect("Unable to remove the temporary file");
        collected
            .iter()
            .for_each(|f| fs::remove_file(f).expect("Unable to remove file"))
    }

    fn geoms_to_global_indices(
        &self,
        geoms: BTreeMap<i64, Vec<(f64, f64, f64)>>,
    ) -> BTreeMap<i64, Index2d> {
        let idx_global: BTreeMap<_, _> = geoms
            .par_iter()
            .map(|(pid, p)| {
                let point: Coordinates = Coordinates {
                    x: p[0].0,
                    y: p[0].1,
                };
                (*pid, self.geo_to_global_rc(point))
            })
            .collect();
        idx_global
    }

    fn geo_to_global_rc(&self, point: Coordinates) -> Index2d {
        let gt = self.metadata.geo_transform.to_array();
        let row: usize = ((point.y - gt[3]) / gt[5]) as usize;
        let col: usize = ((point.x - gt[0]) / gt[1]) as usize;
        Index2d { col, row }
    }
    fn block_id_rowcol(&self, pid: i64, index: Index2d) -> (usize, (i64, Index2d)) {
        // find the block id for a global index. Returns block_id, (point_id, row_col)
        let id = self.id_from_indices(index);
        let row_col = self.global_rc_to_block_rc(index);
        (id, (pid, row_col))
    }

    fn global_rc_to_block_rc(&self, global_index: Index2d) -> Index2d {
        let mut block_col = global_index.col as isize % self.metadata.block_size.cols as isize;
        let mut block_row = global_index.row as isize % self.metadata.block_size.rows as isize;

        let block_col_ov = block_col + self.metadata.overlap_size as isize;
        let block_row_ov = block_row + self.metadata.overlap_size as isize;

        if (global_index.col as isize - block_col_ov) > 0 {
            block_col = block_col_ov;
        } else {
        };

        if global_index.row as isize - block_row_ov > 0 {
            block_row = block_row_ov;
        } else {
        };

        Index2d {
            col: block_col as usize,
            row: block_row as usize,
        }
    }

    fn id_from_indices(&self, index: Index2d) -> usize {
        let n_block_cols = self.n_block_cols();
        (index.col / self.metadata.block_size.cols)
            + (index.row / self.metadata.block_size.rows) * n_block_cols
    }
    fn n_block_cols(&self) -> usize {
        let cols = self.metadata.shape.cols;
        let block_size = self.metadata.block_size;
        round::ceil(cols as f64 / block_size.cols as f64, 0) as usize
    }

    /// Stack RasterDataset along a dimension.
    ///
    /// # Example:
    /// ```
    /// use std::path::PathBuf;
    /// use eorst::{RasterDatasetBuilder,DataSourceBuilder, utils::Dimension};
    /// let src_fn = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif");
    /// let source_1 = DataSourceBuilder::from_file(&src_fn).build();
    /// let source_2 = source_1.clone();
    /// let mut rds_1 = RasterDatasetBuilder::from_source(&source_1)
    ///     .build();    // [1,6,rows,cols]
    /// let rds_2 = RasterDatasetBuilder::from_source(&source_2)
    ///    .build();    // [1,6,rows,cols]
    /// rds_1.stack(&rds_2, Dimension::Layer);
    ///
    /// let block_id = 0;
    /// let data = rds_1.read_block::<i32>(block_id);
    /// assert_eq!(data.shape(), [1, 12, 1024, 1024]);
    /// ```
    ///
    /// **Note**: this is an efficent way to combine data of **same data type**; if you need to use RasterDatasets with
    /// different datatype using zip would be better. Consider using [RasterDataset::reduce_with_mask] or [RasterDataset::reduce_row_pixel_with_mask]

    pub fn stack(
        &mut self,
        other: &RasterDataset,
        dimension_to_stack: Dimension,
    ) -> &mut RasterDataset {
        let axis = dimension_to_stack.get_axis();
        let max_dim_self = self.metadata.shape[axis];
        self.metadata
            .shape
            .stack(other.metadata.shape, dimension_to_stack);
        //Add layers
        for other_layer in &other.metadata.layers {
            let mut layer = other_layer.clone();
            layer.stack_position(other_layer.to_owned(), dimension_to_stack, max_dim_self);
            self.metadata.layers.push(layer);
        }

        self
    }

    /// Extend a RasterDataset
    ///
    /// Example:
    /// 
    /// 
    /// ```rust
    /// 
    /// use std::path::PathBuf;
    /// use eorst::{DataSource, RasterDatasetBuilder,DataSourceBuilder, utils::Dimension};
    /// 
    /// let source = DataSource {
    ///     source: "data/cemsre_t55jfm_20200614_sub_abam5.tif".to_string(),
    ///     bands: [1, 2, 3, 4, 5, 6].to_vec(),
    ///     };
    ///let source_2 = source.clone();
    ///let mut rds_1 = RasterDatasetBuilder::from_source(&source)
    ///     .band_composition_dimension(Dimension::Layer)
    ///     .build();  // [1,6,rows,cols]
    ///let rds_2 = RasterDatasetBuilder::from_source(&source_2)
    ///     .band_composition_dimension(Dimension::Layer)
    ///     .build();  // [1,6,rows,cols]
    /// rds_1.extend(&rds_2);
    /// let block_id = 0;
    /// let data = rds_1.read_block::<i32>(block_id);
    /// assert_eq!(data.shape(), [2, 6, 1024, 1024]);
    /// ```
    /// **Note**: this is an efficent way to combine data of **same data type**; if you need to use RasterDatasets with
    /// different datatype using zip would be better. Consider using [RasterDataset::reduce_with_mask] or [RasterDataset::reduce_row_pixel_with_mask]
    pub fn extend(&mut self, other: &RasterDataset) -> &mut RasterDataset {
        self.metadata.shape.extend(other.metadata.shape);
        //Add layers
        for layer in &other.metadata.layers {
            self.metadata.layers.push(layer.to_owned());
        }
        self
    }

    /// Samples a RasterDataset with a vector of points.
    /// 
    /// The returned value can be either the Value if using `SamplingMethod::Value` the pixel or the Mode
    /// of n (buffer_size*buffer_suze) pixels surrounding the points is using `SamplingMethod::Mode`
    /// 
    /// # Example:
    /// 
    /// ```rust
    /// use std::path::PathBuf;
    /// use std::collections::BTreeMap;
    /// use eorst::{RasterDatasetBuilder,DataSourceBuilder, utils::{Dimension, SamplingMethod}};
    /// 
    /// let raster_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif");
    /// let vector_path = PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.gpkg");
    /// let data_source = DataSourceBuilder::from_file(&raster_path)
    ///          .bands(vec![1])
    ///          .build();
    /// let rds = RasterDatasetBuilder::from_source(&data_source)
    ///          .build();
    /// let data = rds.extract_blockwise(&vector_path, "id", SamplingMethod::Value, None);
    ///
    /// let mut expected: BTreeMap<i16, Vec<i16>> = BTreeMap::new();
    /// expected.insert(1, vec![1910]);
    /// expected.insert(2, vec![957]);
    /// expected.insert(3, vec![1680]);
    /// expected.insert(4, vec![1847]);
    /// expected.insert(5, vec![1746]);
    /// expected.insert(6, vec![624]);
    /// expected.insert(7, vec![818]);
    /// expected.insert(8, vec![1103]);
    ///
    /// assert_eq!(expected, data);
    /// ```
    /// 
    pub fn extract_blockwise(
        &self,
        vector_path: &PathBuf,
        id_col_name: &str,
        method: SamplingMethod,
        buffer_size: Option<usize>,
    ) -> BTreeMap<i16, Vec<i16>> {
        let buffer_size = buffer_size.unwrap_or(0);
        assert!(
            buffer_size <= self.metadata.overlap_size,
            "Buffer size has to be > overlap size"
        );

        assert!(
            self.metadata.shape.times == 1,
            "RasterDatasets with Time component nor supported yet"
        );

        let vector_dataset = Dataset::open(Path::new(vector_path)).unwrap();
        let mut layer = vector_dataset.layer(0).unwrap();
        let mut geoms = BTreeMap::new();

        for feature in layer.features() {
            let geom = feature.geometry().get_point_vec();
            let pid = feature
                .field(id_col_name)
                .unwrap()
                .unwrap()
                .into_int64()
                .unwrap();
            geoms.insert(pid, geom);
        }
        // Find global index for each point. BtreeMap<point_id, global_index>
        let idx_global = self.geoms_to_global_indices(geoms);

        let id_indices: Vec<(usize, (i64, Index2d))> = idx_global
            .par_iter()
            .map(|(pid, index)| self.block_id_rowcol(*pid, *index))
            .collect();

        let block_ids: Vec<_> = idx_global
            .par_iter()
            .map(|(_, index)| self.id_from_indices(*index))
            .collect();

        let blocks_to_process: Vec<_> = block_ids.iter().unique().collect();

        let pool = rayon::ThreadPoolBuilder::new().build().unwrap();
        let handle = pool.install(|| {
            blocks_to_process
                .into_par_iter()
                .map(|id| -> (Vec<usize>, Vec<usize>, Vec<Vec<i16>>) {
                    //id is block id, not point id
                    // let id = *id;
                    // print!("block id: {:?}.", id);
                    let mut pos: Vec<Index2d> = Vec::new();
                    let mut idx: Vec<usize> = Vec::new();
                    let mut pids: Vec<usize> = Vec::new();
                    for (pid, p) in id_indices.iter().enumerate() {
                        if p.0 == *id {
                            pos.push(Index2d {
                                col: p.1 .1.col,
                                row: p.1 .1.row,
                            });
                            pids.push(pid);
                            idx.push(p.1 .0 as usize);
                        } else {
                        };
                    }

                    let mut res = Vec::new();
                    let rect: Rectangle = Rectangle {
                        left: buffer_size,
                        top: buffer_size,
                        right: buffer_size,
                        bottom: buffer_size,
                    };

                    // let block_attributes = self.block_from_id(*id);
                    // let read_window = block_attributes.read_window;
                    // let overlap = block_attributes.overlap;
                    let data = self.read_block(*id);
                    let bands = data.shape()[1];
                    for band_n in 0..bands {
                        let mut res_band = Vec::new();
                        let band_data = data.slice(s![0_i32, band_n, .., ..]);
                        for (_, point) in pos.iter().enumerate() {
                            let point_shifted = Index2d {
                                col: point.col + self.blocks[*id].overlap.left,
                                row: point.row + self.blocks[*id].overlap.top,
                            };
                            match method {
                                SamplingMethod::Mode => {
                                    let mut window_data: Vec<i16> =
                                        rect_view(&band_data, rect, point_shifted)
                                            .iter()
                                            .copied()
                                            .collect();
                                    window_data.sort_by(|a, b| a.partial_cmp(b).unwrap());
                                    let median = window_data[window_data.len() / 2];
                                    res_band.push(median);
                                }
                                SamplingMethod::Value => {
                                    // println!("Band data: {:?}", band_data);
                                    let d = band_data[(point_shifted.row, point_shifted.col)];
                                    res_band.push(d);
                                }
                            }
                        }
                        res.push(res_band);
                    }
                    (pids, idx, res)
                })
        });

        let collected: Vec<_> = handle.collect();
        let pids: Vec<_> = collected.iter().map(|(pid, _, _)| pid).collect();
        let vals: Vec<_> = collected.iter().map(|(_, _, vals)| vals).collect();
        let idxs: Vec<_> = collected.iter().map(|(_, idx, _)| idx).collect();
        let mut results = BTreeMap::new();

        let num_bands = vals[0].len();
        let num_blocks = pids.len();
        for block in 0..num_blocks {
            for i in 0..pids[block].len() {
                let mut vals_point: Vec<i16> = Vec::new();
                let id = idxs[block][i];
                for band in 0..num_bands {
                    vals_point.push(vals[block][band][i]);
                }
                let mut res_point = BTreeMap::new();
                res_point.insert(id as i16, vals_point);
                results.append(&mut res_point);
            }
        }

        results
    }
    #[doc(hidden)]
    pub fn read_block<T>(&self, block_id: usize) -> RasterData<T>
    where
        T: gdal::raster::GdalType + Copy + std::fmt::Debug + num_traits::identities::Zero,
    {
        // the shape of the out array can be constructed
        // of the two first indices in shelf.shape and the size
        // attribute of the read window.
        let block = &self.blocks[block_id];
        let read_window = block.read_window;
        let overlap = block.overlap;

        let data_shape = (
            self.metadata.shape.times,
            self.metadata.shape.layers,
            read_window.size.rows as usize + overlap.top + overlap.bottom,
            read_window.size.cols as usize + overlap.left + overlap.right,
        );
        let mut data: RasterData<T> = RasterData::zeros(data_shape);

        for layer in &self.metadata.layers {
            let raster_path = Path::new(&layer.source);
            let band_index: isize = 1;
            let ds = Dataset::open(&raster_path).unwrap();
            let raster_band = ds.rasterband(band_index).unwrap();
            let window = (read_window.offset.cols, read_window.offset.rows);
            let window_size = (
                read_window.size.cols as usize,
                read_window.size.rows as usize,
            );
            let array_size = window_size;
            let e_resample_alg = None;
            let layer_data = raster_band
                .read_as_array::<T>(window, window_size, array_size, e_resample_alg)
                .unwrap();

            let slice = s![
                layer.time_pos,
                layer.layer_pos,
                overlap.top..read_window.size.rows as usize + overlap.top,
                overlap.left..read_window.size.cols as usize + overlap.left,
            ];

            data.slice_mut(slice).assign(&layer_data);

            if overlap.top > 0 {
                let new_slice = s![.., .., block.overlap_size..2 * block.overlap_size, ..];
                let new_data = data.slice(new_slice).to_owned();
                let data_slice = s![.., .., 0..block.overlap_size, ..];
                data.slice_mut(data_slice).assign(&new_data);
            }
            if overlap.bottom > 0 {
                let max_row = data.shape()[2];
                let new_slice = s![
                    ..,
                    ..,
                    max_row - 2 * block.overlap_size..max_row - block.overlap_size,
                    ..
                ];
                let new_data = data.slice(new_slice).to_owned();
                let data_slice = s![.., .., max_row - block.overlap_size..max_row, ..];
                data.slice_mut(data_slice).assign(&new_data);
            }

            if overlap.left > 0 {
                let new_slice = s![.., .., .., block.overlap_size..2 * block.overlap_size];
                let new_data = data.slice(new_slice).to_owned();
                let data_slice = s![.., .., .., 0..block.overlap_size];
                data.slice_mut(data_slice).assign(&new_data);
            }

            if overlap.right > 0 {
                let max_col = data.shape()[3];
                let new_slice = s![
                    ..,
                    ..,
                    ..,
                    max_col - 2 * block.overlap_size..max_col - block.overlap_size
                ];
                let new_data = data.slice(new_slice).to_owned();
                let data_slice = s![.., .., .., max_col - block.overlap_size..max_col];
                data.slice_mut(data_slice).assign(&new_data);
            }
        }
        data
    }
}

impl<'a> Iterator for RasterDatasetIter<'a> {
    type Item = Self;

    fn next(&mut self) -> Option<Self::Item> {
        if self.iter_index >= self.parent.blocks.len() {
            None
        } else {
            let item = RasterDatasetIter {
                parent: self.parent,
                block: &self.parent.blocks[self.iter_index],
                iter_index: self.iter_index,
            };
            self.iter_index += 1;
            Some(item)
        }
    }
}

impl<'a> IntoIterator for &'a RasterDatasetIter<'_> {
    type Item = RasterDatasetIter<'a>;
    type IntoIter = RasterDatasetIter<'a>;

    fn into_iter(self) -> Self::IntoIter {
        RasterDatasetIter {
            parent: self.parent,
            block: &self.parent.blocks[self.iter_index],
            iter_index: self.iter_index,
        }
    }
}

impl RasterBlock {
    pub fn write_samples_feature<T, P>(&self, data: &Array2<T>, file_name: P)
    where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + Debug,
        P: AsRef<Path> + Copy + std::convert::AsRef<std::ffi::OsStr>,
    {
        let n_bands = data.shape()[1] as isize;
        let block_geotransform = self.geo_transform;
        let epsg_code = self.epsg_code;
        let size_x = self.read_window.size.cols as usize;
        let size_y = self.read_window.size.rows as usize;
        let block_size: BlockSize = BlockSize {
            rows: size_y,
            cols: size_x,
        };

        raster_from_size::<T, P>(
            file_name,
            block_geotransform,
            epsg_code as u32,
            block_size,
            n_bands,
        );
        let ds_options = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            ..DatasetOptions::default()
        };
        let out_ds = Dataset::open_ex(Path::new(&file_name), ds_options).unwrap();

        // each feature goes to a band
        for feature in 1..n_bands + 1 {
            let mut out_band = out_ds.rasterband(feature).unwrap();
            let out_data = data.slice(s![.., feature - 1]);
            let out_data_u8: Vec<T> = out_data.into_iter().map(|v| *v as T).collect();
            let data_buffer = Buffer {
                size: (size_x, size_y),
                data: out_data_u8,
            };
            out_band
                .write((0, 0), (size_x, size_y), &data_buffer)
                .unwrap();
        }
    }

    pub fn write3<T, P>(&self, data: Array3<T>, out_fn: P)
    where
        T: gdal::raster::GdalType + Copy + num_traits::identities::Zero + Debug,
        P: AsRef<Path> + Copy,
    {
        // create and empty raster with the right size
        let block_geotransform = self.geo_transform;
        let block_size: BlockSize = BlockSize {
            rows: self.read_window.size.rows as usize,
            cols: self.read_window.size.cols as usize,
        };
        let dataset_options: DatasetOptions = DatasetOptions {
            open_flags: GdalOpenFlags::GDAL_OF_UPDATE,
            allowed_drivers: None,
            open_options: None,
            sibling_files: None,
        };
        let n_bands = data.shape()[0] as isize;
        let epsg_code = self.epsg_code;
        raster_from_size::<T, P>(
            out_fn,
            block_geotransform,
            epsg_code.try_into().unwrap(),
            block_size,
            n_bands,
        );
        let out_ds = Dataset::open_ex(out_fn, dataset_options).unwrap();
        for band in 0..data.shape()[0] {
            let b = (band + 1) as isize;
            let mut out_band = out_ds.rasterband(b).unwrap();
            let data_vec: Vec<T> = data.slice(s![band, .., ..]).into_iter().copied().collect();
            let data_buffer = Buffer {
                size: (block_size.cols, block_size.rows),
                data: data_vec,
            };

            out_band
                .write((0, 0), (block_size.cols, block_size.rows), &data_buffer)
                .unwrap();
        }
    }
}

#[allow(dead_code)]
pub(crate) fn array2_to_nested_vec<T: std::clone::Clone>(arr: &Array2<T>) -> Vec<Vec<T>> {
    let mut res: Vec<Vec<T>> = Vec::new();
    arr.axis_iter(Axis(0))
        .for_each(|row| res.push(row.to_vec()));
    res
}
#[allow(dead_code)]
fn get_class(dataset_path: &PathBuf, id_column: &str, class_column: &str) -> BTreeMap<i16, i16> {
    let dataset = Dataset::open(dataset_path).unwrap();
    let mut layer = dataset.layer(0).unwrap();

    let _fields_defn = layer
        .defn()
        .fields()
        .map(|field| (field.name(), field.field_type(), field.width()))
        .collect::<Vec<_>>();
    let mut class: BTreeMap<i16, i16> = BTreeMap::new();
    for feature in layer.features() {
        let id = feature
            .field(id_column)
            .unwrap()
            .unwrap()
            .into_int()
            .unwrap();
        let condition = feature
            .field(class_column)
            .unwrap()
            .unwrap_or(FieldValue::IntegerValue(-1))
            .into_int()
            .unwrap();
        class.insert(id as i16, condition as i16);
    }
    class
}

impl Layer {
    pub fn stack_position(
        &mut self,
        original_layer: Layer,
        dimension_to_stack: Dimension,
        max_dim: usize,
    ) -> &mut Layer {
        match dimension_to_stack {
            Dimension::Layer => self.layer_pos = original_layer.layer_pos + max_dim,
            Dimension::Time => self.time_pos = original_layer.time_pos + max_dim,
        }
        self
    }
}
