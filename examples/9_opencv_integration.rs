use eorst::{
    utils::{arrayview2_to_mat, mat_to_arrayview2, Dimension, RasterData},
    DataSourceBuilder, RasterDatasetBuilder,
};
use ndarray::{s, Array, Array2, Array3, ArrayView2, Axis};
use opencv::core::{Mat, Scalar};
use opencv::{imgproc, prelude::DataType};
use std::path::{Path, PathBuf};

fn edge_density(data: &ArrayView2<i16>) -> Array2<i16> {
    let rows = data.shape()[0];
    let cols = data.shape()[1];
    // let data = data.mapv(|x| (x/12) as u8);
    let data_mat = arrayview2_to_mat(data.view()); //.mapv(|x| x / 10).view());
                                                   // create the mat to store the output
    let mut data_edge = arrayview2_to_mat::<i16>(Array2::zeros((rows, cols)).view());
    imgproc::laplacian(&data_mat, &mut data_edge, 3, 13, 0.000001, 0., 1).unwrap();
    mat_to_arrayview2(data_edge)
}

fn worker(data: &RasterData<i16>, dimension: Dimension) -> Array3<i16> {
    let mut result: Array3<i16> =
        Array::zeros((0, data.shape()[2] as usize, data.shape()[3] as usize));

    let ed_red = edge_density(&data.slice(s![0_usize, 0_usize, .., ..]));

    result.push(Axis(0), (ed_red).view()).unwrap();
    result
}

fn main() {
    let data_source =
        DataSourceBuilder::from_file(&PathBuf::from("data/cemsre_t55jfm_20200614_sub_abam5.tif"))
            .bands(vec![3])
            .build();
    let rds = RasterDatasetBuilder::from_source(&data_source)
        .block_size(BlockSize {
            rows: 256,
            cols: 256,
        })
        .overlap_size(6)
        .build();

    let out_fn = PathBuf::from("9_opencv_integration.tif");

    rds.reduce::<i16>(worker, Dimension::Layer, 4, &out_fn);
}
