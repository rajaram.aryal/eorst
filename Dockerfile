ARG PROD_CONTAINER_REGISTRY
FROM ${PROD_CONTAINER_REGISTRY}/builder_rust:latest as builder

ADD . eorst/
RUN cargo install --path eorst

FROM ubuntu:bionic
COPY --from=builder  /opt/cargo/bin/eorst /opt/bin/eorst
COPY --from=builder /usr/lib/ /usr/lib/
COPY --from=builder /usr/lib/x86_64-linux-gnu/ /usr/lib/
COPY --from=builder /lib/ /lib/

